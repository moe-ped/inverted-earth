﻿using UnityEngine;

public class Rotate : MonoBehaviour
{

	[SerializeField]
	float Speed;

	void Update ()
	{
		transform.Rotate( 0, Speed * Time.deltaTime, 0 );
	}
}
