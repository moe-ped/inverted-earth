﻿using UnityEngine;

public class CountWater : MonoBehaviour
{

	[SerializeField]
	private Texture2D Heightmap1;
	[SerializeField]
	private Texture2D Heightmap2;
	[SerializeField]
	private float SeaLevel1 = 0.5f;
	[SerializeField]
	private float SeaLevel2 = 0.5f;

	void Update ()
	{
		if ( Input.GetKeyDown( KeyCode.T ) ) {
			Debug.Log( getWaterAmount( Heightmap1, SeaLevel1 ) + "\n" + getWaterAmount( Heightmap2, SeaLevel2 ) );
		}
	}

	private float getWaterAmount ( Texture2D heightmap, float seaLevel )
	{
		float amount = 0;
		Color[] pixels = heightmap.GetPixels();
		foreach ( Color pixel in pixels ) {
			amount += Mathf.Clamp( seaLevel - pixel.grayscale, 0, 1 );
		}
		return amount;
	}
}
