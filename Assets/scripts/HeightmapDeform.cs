﻿using System.Collections.Generic;
using UnityEngine;

public class HeightmapDeform : MonoBehaviour
{
	[SerializeField]
	private Texture2D Heightmap;
	[SerializeField]
	private float Intensity = 0.1f;
	[SerializeField]
	private int SubdivisionsX = 256;
	[SerializeField]
	private int SubdivisionsY = 128;

	private Mesh Mesh;
	private List<Vector3> Vertices;
	private List<int> Triangles;
	private List<Vector2> Uv;

	void Start ()
	{
		LoadMesh();
		CreateMesh();
		MoveVertices();
		SaveMesh();
	}

	private void LoadMesh ()
	{
		Mesh = GetComponent<MeshFilter>().mesh;
		Vertices = new List<Vector3>( Mesh.vertices );
		Uv = new List<Vector2>( Mesh.uv );
		Triangles = new List<int>( Mesh.triangles );
	}

	private void CreateMesh ()
	{
		Mesh = new Mesh();

		for ( int y = 0; y < SubdivisionsY; y++ ) {
			for ( int x = 0; x < SubdivisionsX; x++ ) {
				int i = Mathf.FloorToInt( y * SubdivisionsX + x );
				float xPercentage = ((float)x) / (SubdivisionsX - 1);
				float yPercentage = ((float)y) / (SubdivisionsY - 1);
				Vector3 position = new Vector3( 0.5f, 0, 0.5f );
				position = Quaternion.AngleAxis( -xPercentage * 360, Vector3.up ) * position;
				position = Quaternion.AngleAxis( yPercentage * 180 - 90, Vector3.Cross( position, Vector3.up ) ) * position;
				// TODO: skip unnecessary vertices on first and last rows
				Vertices.Add( position );
				Uv.Add( new Vector2( xPercentage, yPercentage ) );

				int up = i - SubdivisionsX;
				int upRight = up + 1;
				int right = i + 1;

				// TODO: skip unnecessary tris on first and last rows
				if ( up < 0 || right >= SubdivisionsX * SubdivisionsY )
					continue;
				if ( right % SubdivisionsX == 0 ) {
					right -= SubdivisionsX;
					upRight -= SubdivisionsX;
				}

				Triangles.Add( up );
				Triangles.Add( i );
				Triangles.Add( upRight );

				Triangles.Add( upRight );
				Triangles.Add( i );
				Triangles.Add( right );
			}
		}
	}

	private void SaveMesh ()
	{
		Mesh.vertices = Vertices.ToArray();
		Mesh.triangles = Triangles.ToArray();
		Mesh.uv = Uv.ToArray();
		Mesh.RecalculateNormals();
		GetComponent<MeshFilter>().mesh = Mesh;

	}

	private void MoveVertices ()
	{
		for ( int i = 0; i < Vertices.Count; i++ ) {
			Vector3 vertex = Vertices[i];
			Vector2 uv = Uv[i];

			int x = Mathf.FloorToInt( Heightmap.width * uv.x );
			int y = Mathf.FloorToInt( Heightmap.height * uv.y );
			float height = Heightmap.GetPixel( x, y ).grayscale;
			Vector3 offset = vertex * height * Intensity;

			vertex += offset;
			Vertices[i] = vertex;
		}
	}
}
